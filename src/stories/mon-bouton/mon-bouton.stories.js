// mon-bouton.stories.js
import { html } from 'lit';
import './mon-bouton.js';

export default {
  tags: ['autodocs'],
  title: 'Components/MonBouton',
  component: 'mon-bouton',
  argTypes: {
    label: {
      control: 'text',
      description: 'Texte du bouton',
      defaultValue: 'Valider'
    },
    'bouton-click': {
      action: 'clicked',
      description: 'Déclenché quand le bouton est cliqué'
    }
  }
};

// Story de base
export const Basic = (args) => html`
  <mon-bouton 
    label=${args.label}
    @bouton-click=${args['bouton-click']}
  ></mon-bouton>
`;

Basic.args = {
  label: 'Valider'
};

// Story avec un exemple plus complet
export const WithCallback = (args) => {
  const handleClick = (e) => {
    console.log('Bouton cliqué dans Storybook!');
    args['bouton-click'](e);
  };

  return html`
    <mon-bouton 
      label=${args.label}
      @bouton-click=${handleClick}
    ></mon-bouton>
  `;
};

WithCallback.args = {
  label: 'Cliquez-moi'
};