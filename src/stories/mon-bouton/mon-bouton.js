// mon-bouton.js
import { LitElement, html } from 'lit';

/**
 * Wraps a `<button>` with a skeleton state, some modes and a delay mechanism.
 *
 * ## Details
 *
 * * Attributes `primary`, `success`, `warning` and `danger` define the UI _mode_ of the button.
 * * They are exclusive, you can only set one UI _mode_ at a time.
 * * When you don't use any of these values, the default UI _mode_ is `simple`.
 *
 * ## Link appearance
 *
 * * In some cases (to be defined/explained later), you need a button with a click handler that looks like a link.
 * * Don't use a `<a>` without an href and use our `<cc-button link>` instead.
 * * When `link` is enabled, the following properties won't have any effect: `primary`, `success`, `warning`, `danger`, `outlined`, `delay`.
 *
 * ## Delay mechanism
 *
 * * When `delay` is set, `cc-button:click` events are not fired immediately.
 * * They are fired after the number of seconds set with `delay`.
 * * During this `delay`, the user is presented a "click to cancel" label.
 * * If the user clicks on "click to cancel", the `cc-button:click` event is not fired.
 * * If the button `disabled` mode is set during the delay, the `cc-button:click` event is not fired.
 * * If you set `delay=0`, the button will have the same width as other buttons with delay, but the event will be triggered instantly.
 *
 * @cssdisplay inline-block
 *
 * @fires {CustomEvent} cc-button:click - Fires whenever the button is clicked.<br>If `delay` is set, fires after the specified `delay` (in seconds).
 *
 * @slot - The content of the button (text or HTML). If you want an image, please look at the `image` attribute.
 * @cssprop {BorderRadius} --cc-button-border-radius - Sets the value of the border radius CSS property (defaults: `0.15em`).
 * @cssprop {FontWeight} --cc-button-font-weight - Sets the value of the font weight CSS property (defaults: `bold`).
 * @cssprop {TextTransform} --cc-button-text-transform - Sets the value of the text transform CSS property (defaults: `uppercase`).
 */
export class MonBouton extends LitElement {
  static properties = {
    label: { type: String },
  };

  onClick(e) {
    this.dispatchEvent(new CustomEvent('bouton-click', {
        bubbles: true,
        composed: true,
        detail: {
            monEvent: e,
            conteneur: 1,
        }
    }));
  }

  render() {
    return html`
      <button @click=${this.onClick}>
        ${this.label}
      </button>
    `;
  }
}

customElements.define('mon-bouton', MonBouton);