import { html, css, LitElement } from 'lit';

export class PcButton extends LitElement {
    static get properties() {
        return {
            id: { type: String},
            label: { type: String },
            title: { type: String },
            icon_path: { type: String },
            css_classes: { type: String }
        };
    }
    
    static get styles() {
        return [
            css`
                .button {
                    cursor: pointer;
                    text-decoration: none;
                    background: none;
                    color: var(--colors-text-inverted);
                    border: var(--button-border-thickness) solid transparent;
                    border-radius: var(--radius-sm);
                    box-sizing: content-box;
                }

                .button-label-padding {
                    padding: 0 var(--spacing-2x) 0 var(--spacing-1-5x);
                }

                .button svg g path {
                    fill: var(--colors-text-inverted-brand);
                }

                .button-content {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    gap: var(--spacing-1x);
                    height: var(--button-sm-height);
                    padding: 0 var(--spacing-2x) 0 var(--spacing-1-5x);
                    border: var(--button-border-thickness) solid var(--colors-border-action-high-brand);
                    border-radius: var(--radius-sm);
                    background-color: var(--colors-background-action-high);
                    box-sizing: content-box;
                }

                .button:focus, .button.button--tertiary:focus {
                    border: var(--button-border-thickness) dashed var(--colors-border-hover-brand);
                    outline: none;
                }

                .button:hover, .button.button--tertiary:hover {
                    border: var(--button-border-thickness) solid transparent;
                    transition: border var(--widget-animation);
                }

                .button:hover .button-content {
                    background-color: var(--colors-background-hover);
                    border-color: var(--colors-background-hover);
                    transition: border-color var(--widget-animation);
                    transition: background-color var(--widget-animation);
                }

                .button:active .button-content {
                    background-color: var(--colors-background-cliqued);
                    border-color: var(--colors-background-cliqued);
                }

                .button.button--secondary .button-content {
                    color: var(--colors-text-action-high-brand);
                    background-color: transparent;
                }

                .button.button--secondary:hover .button-content {
                    background-color: var(--colors-background-hover-alt);
                    border-color: var(--colors-border-action-high-brand);
                    transition: border-color var(--widget-animation);
                    transition: background-color var(--widget-animation);
                }

                .button.button--secondary:active .button-content {
                    background-color: var(--colors-background-cliqued-alt);
                }

                .button.button--secondary svg g path {
                    fill: var(--colors-text-action-high-brand);
                }
            `
        ]
    }

    render() {
        let css_class_button = '';
        let label_button = '';
        if (this.label) {
            css_class_button = 'button-label-padding';
            label_button = html`<div class="button-label">${this.label}</div>`;
        }
        return html`
        <a id="${this.id}" class="button ${this.css_classes}" href="" tabindex="1" title="${this.title}">
            <div class="button-content ${this.css_class_button}">
            <div class="button__icon">
                <img src="${this.icon_path}" />
            </div>
            ${label_button}
            </div>
        </a>
      `;
    }
}

window.customElements.define('pc-button', PcButton);