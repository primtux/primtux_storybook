import { html, css, LitElement } from 'lit';

export class PcSessionList extends LitElement {
    static get properties() {
        return {
            title: String,
            session: JSON,

        };
    }

    static get styles() {
        return [
            css`
            h1 {
                text-align: center;
            }

            .session-choice {
                flex-grow: 1;
                display: flex;
                height: 100vh;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                font-size: var(--spacing-4x);
            }

            .session-choice-card {
                display: flex;
                flex-direction: row;
                align-items: center;
                text-align: center;
            }
            `
        ]
    }

    render() {
        let session_content = '';
        for (const key in this.sessions) {
            session_content = html`
                ${session_content}
                <pc-session-tile session_name="${this.sessions[key].session_name}" icon_path="${this.sessions[key].icon_path}"></pc-session-tile>
            `;
        }
        let html_title = '';
        if (this.title) {
            html_title = html`<h1>${this.title}</h1>`;
        }
        return html`
            <div class="session-choice">
                ${html_title}
                <div class="session-choice-card">
                    ${session_content}
                </div>
            </div>
       `;
    }
}

window.customElements.define('pc-session-list', PcSessionList);