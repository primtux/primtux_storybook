import './pc-session-list.js';

import jerry_session__svg from '../assets/sessions/jerry.svg';
import koda_session__svg from '../assets/sessions/koda.svg';
import leon_session__svg from '../assets/sessions/leon.svg';
import poe_session__svg from '../assets/sessions/poe.svg';

export default {
    tags: ['autodocs'],
    title: '🧬 Molecules/<pc-session-list>',
    component: 'pc-session-list'
};

export const session_primtuxmenu = {
    args: {
        title: "Quel personnage ?",
        sessions: [
            {
                icon_path: jerry_session__svg,
                session_name: "Jerry"
            },
            {
                icon_path: koda_session__svg,
                session_name: "Koda"
            },
            {
                icon_path: leon_session__svg,
                session_name: "Léon"
            },
            {
                icon_path: poe_session__svg,
                session_name: "Poe"
            },
        ]
    },
};
