import './pc-app.js';

import achat_app from '../assets/apps/achats.png';

export default {
    tags: ['autodocs'],
    title: '🧬 Atoms/<pc-app>',
    component: 'pc-app',
    argTypes: {
        name: { type: 'string' },
        icon_path: {
            control: { type: 'select' },
            options: [
                achat_app
            ],
        },
        is_installed: { type: 'boolean' },
    }
};

export const primary = {
    args: {
        name: 'Achats',
        icon_path: achat_app
    },
};