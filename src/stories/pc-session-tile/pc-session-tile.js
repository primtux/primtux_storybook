import { html, css, LitElement } from 'lit';

export class PcSessionTile extends LitElement {
    static get properties() {
        return {
            icon_path: { type: String},
            session_name: { type: String },
            onClick: { type: Function },
        };
    }

    handleClick(element) {
        if (this.onClick) {
            this.onClick(element);
        }
    }

    static get styles() {
        return [
            css`
            .session-link {
                display: flex;
                flex-direction: column;
                align-items: center;
                width: fit-content;
            }

            .session-link--sm {
                border-radius: var(--radius-sm);
                overflow: hidden;
                border: var(--button-border-thickness) solid var(--colors-background-default);
            }

            .menu__icon {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: end;
                width: var(--spacing-7x);
                height: var(--spacing-7x);
                margin-right: var(--spacing-1x);
                background-color: var(--colors-background-alt-brand);
                border-radius: var(--spacing-3x);
                border: var(--spacing-0-5x) solid var(--colors-background-default);
                overflow: hidden;
            }

            .menu__icon--hug {
                height: 12.5rem;
                width: 12.5rem;
                margin: 0 var(--spacing-4x);
            }

            .menu__icon--sm {
                height: var(--button-sm-height);
                width: var(--spacing-5x);
                margin: 0;
                border: 0;
                border-radius: var(--radius-sm); 
            }

            .menu__icon--sm img {
                width: 100%;
            }

            .menu__icon-label {
                color: var(--colors-text-action-high-brand);
                font-size: var(--spacing-3x);
                padding: var(--spacing-2x) var(--spacing-3x);
            }

            .menu__icon-label--hug {
                border-radius: 0 0 var(--spacing-4x) var(--spacing-4x);
            }

            .menu__icon-label--sm {
                font-size: var(--spacing-2x);
                padding: var(--spacing-1x) var(--spacing-1-5x);
                line-height: var(--spacing-3x);
            }

            .session-link:focus {
                outline: none;
            }

            .session-link:focus .menu__icon--hug {
                border-color: var(--colors-background-hover);
                border-style: dashed;
            }

            .session-link:hover .menu__icon--hug {
                cursor: pointer;
                border-color: var(--colors-background-hover);
                border-style: solid;
                transition: border-color var(--widget-animation);
            }

            .session-link:hover .menu__icon-label--hug {
                cursor: pointer;
                color: var(--colors-text-inverted-brand);
                background-color: var(--colors-background-hover);
                transition: color var(--widget-animation);
                transition: background-color var(--widget-animation);
            }

            .session-link--sm:focus {
                border: var(--button-border-thickness) dashed var(--colors-border-hover-brand);
            }

            .session-link--sm:hover {
                background-color: var(--colors-background-hover-alt);
                border: var(--button-border-thickness) solid var(--colors-background-default);
                transition: border-color var(--widget-animation);
                transition: background-color var(--widget-animation);
            }

            .session-link:active .menu__icon--hug {
                border-color: var(--colors-background-cliqued);
                border-style: solid;
            }

            .session-link:active .menu__icon-label--hug {
                background-color: var(--colors-background-cliqued);
            }

            .session-link--sm:active {
                background-color: var(--colors-background-cliqued-alt);
            }
                        `
        ]
    }

    render() {
        let session_label = "";
        if (this.session_name)
        {
            session_label = html`<div class="menu__icon-label menu__icon-label--hug">${this.session_name}</div>`;
        }
        return html`
            <a class="session-link" @click=${this.handleClick(this)}>
                <div class="menu__icon menu__icon--hug">
                    <img src="${this.icon_path}"/>
                </div>
                ${session_label}
            </a>
       `;
    }
}

window.customElements.define('pc-session-tile', PcSessionTile);