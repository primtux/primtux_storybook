import { fn } from '@storybook/test';
import { action } from '@storybook/addon-actions';

import './pc-session-tile.js';

import jerry_session__svg from '../assets/sessions/jerry.svg';
import koda_session__svg from '../assets/sessions/koda.svg';
import leon_session__svg from '../assets/sessions/leon.svg';
import poe_session__svg from '../assets/sessions/poe.svg';

export default {
    tags: ['autodocs'],
    title: '🧬 Atoms/<pc-session-tile>',
    component: 'pc-session-tile',
    args: {
        onClick: fn(),
    },
};

export const jerry = {
    args: {
        icon_path: jerry_session__svg,
        session_name: 'Jerry',
    },
};

export const koda = {
    args: {
        icon_path: koda_session__svg,
        session_name: 'Koda',
    },
};

export const leon = {
    args: {
        icon_path: leon_session__svg,
        session_name: 'Léon',
    },
};

export const poe = {
    args: {
        icon_path: poe_session__svg,
        session_name: 'Poe',
    },
};