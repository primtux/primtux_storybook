import './pc-search-bar.js';

export default {
    tags: ['autodocs'],
    title: '🧬 Atoms/<pc-search-bar>',
    component: 'pc-search-bar'
};

export const primary = {
    args: {
    },
};