---
kind: '🏡 Démarrage'
title: 'Structure'
---

# Structure

Les composants sont des [Web Components](https://developer.mozilla.org/en-US/docs/Web/API/Web_components) créé avec [Lit](https://lit.dev/).

Cette librairie suit l'[atomic design](https://bradfrost.com/blog/post/atomic-web-design/)
