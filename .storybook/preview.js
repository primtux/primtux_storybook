import '../src/styles/design-system-variables.css';
import '../src/styles/design-system-semantic-light.css';
import '../src/styles/design-system-semantic-dark.css';
import '../src/styles/design-system.css';
import '../src/styles/style.css';

import '../src/styles/stories.css';

import { themes } from '@storybook/theming';

// https://storybook.js.org/addons/storybook-dark-mode
export const parameters = {
  current: 'dark',
  darkMode: {
    stylePreview: true,
    // Override the default dark theme
    dark: { ...themes.dark, appBg: '#151D32FF' },
    // Override the default light theme
    light: { ...themes.normal, appBg: 'white' }
  }
};
export const tags = ['autodocs'];
